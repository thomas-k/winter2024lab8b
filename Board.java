import java.util.Random;

public class Board{
	private Tile[][] grid;
	
	//constructor
	public Board(int size){
		Random rng = new Random();
		
		this.grid = new Tile[size][size];
		
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				this.grid[i][j] = Tile.BLANK;
			}
			int x = rng.nextInt(this.grid[i].length);
			grid[i][x] = Tile.HIDDEN_WALL;
		}
	}
	
	// to String
	public String toString(){
		String output = "";
		String filler = "+";
		for(int i = 0; i < this.grid.length; i++){
			filler += "---+";
		}
		
		for(Tile[] t1: this.grid){
			output += "\n" + filler + "\n| ";
			for(Tile t2: t1){
				output += t2.getName() + " | ";
			}
		}
		output += "\n" + filler;
		return output;
	}
	
	public int placeToken(int row, int col){
		row--;
		col--;
		if(row > this.grid.length || col > this.grid.length || col < 0 || row < 0) return -2;
		else if(this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL) return -1;
		else if(this.grid[row][col] == Tile.HIDDEN_WALL){
			this.grid[row][col] = Tile.WALL;
			return 1;
		}
		else {
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
	
	public Tile[][] getGrid(){
		return this.grid;
	}
}