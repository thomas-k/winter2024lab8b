import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		System.out.println(titleScreen());
		System.out.println("Welcome to this game of Castles!");
		System.out.println("In this game, you have 6 castles (♖) you must place on the board below");
		System.out.println("But be careful! On that board, there are 5 hidden walls...");
		System.out.println("If you place a castle on a wall, you lose a turn");
		System.out.println("If you haven't placed your castles by the end of your 8th turn, you lose!");
		System.out.println("You must place your castles carefully. Good luck!\n");
		
		Board b1 = new Board(5);
		
		int numCastles = 6;
		int turns = 0;
		
		// game loop
		while(turns < 8 && numCastles > 0){
			//printwork
			System.out.println("Turns left: " + Integer.toString(8 - turns));
			System.out.println("Castles (♖) left: " + numCastles);
			System.out.println(b1);
			
			//where the player enters the castle position
			System.out.println("Enter the row (1-5) where you choose to place your castle.");
			int row = Integer.parseInt(reader.nextLine());
			System.out.println("Enter the column (1-5) where you choose to place your castle.");
			int col = Integer.parseInt(reader.nextLine());
			int placed = b1.placeToken(row, col);
			
			while(placed < 0){
				System.out.println("Invalid row/column number");
				System.out.println("Please re-enter the row (1-5) where you choose to place your castle.");
				row = Integer.parseInt(reader.nextLine());
				System.out.println("Please re-enter the column (1-5) where you choose to place your castle.");
				col = Integer.parseInt(reader.nextLine());
				placed = b1.placeToken(row, col);
			}
			if(placed == 1){
				turns++;
				System.out.println("There was a wall there!\n");
			}
			if(placed == 0){
				turns++;
				numCastles--;
				System.out.println("Castle sucessfully placed!\n");
			}
		}
		System.out.println(b1);
		if(numCastles == 0) System.out.println("You won! Congrats!");
		else System.out.println("You lost, try again :(");
		
		System.out.println(titleScreen());
	}
	public static String titleScreen(){
		String output = "";
		output += "\n_________                  __  .__                  ";
		output += "\n\\_   ___ \\_____    _______/  |_|  |   ____   ______ ";
		output += "\n/    \\  \\/\\__  \\  /  ___/\\   __\\  | _/ __ \\ /  ___/ ";
		output += "\n\\     \\____/ __ \\_\\___ \\  |  | |  |_\\  ___/ \\___ \\  ";
		output += "\n \\______  (____  /____  > |__| |____/\\___  >____  > ";
		output += "\n        \\/     \\/     \\/                 \\/     \\/  ";
		
		return output;
	}
}