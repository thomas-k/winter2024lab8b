enum Tile{
	BLANK("_"),
	HIDDEN_WALL("_"),
	WALL("W"),
	CASTLE("♖");
	
	private Tile(final String name){
		this.name = name;
	}
	
	private String name;
	
	public String getName(){
		return this.name;
	}
}